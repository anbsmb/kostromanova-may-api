FROM python:3.8-slim-buster
WORKDIR /app
COPY . .
EXPOSE 8100
RUN apt-get update && apt-get -y install libpq-dev gcc && pip install psycopg2 && pip install -U flask-cors
RUN pip3 install -r req.txt
CMD [ "python3", "main.py" ]
